﻿using System;

namespace dz_linq
{
    public class Account
    {
        public int Id { get; set; }
        public DateTime CreateDate { get; set; }
        public decimal CurrentSum { get; set; }
        public int OwnerId { get; set; }
    }
}