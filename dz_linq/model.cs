﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Design;
using System.Globalization;
using System.Linq;

namespace dz_linq
{
    public class model
    {
        public List<User> users { get; set; } //Коллекция Пользователи - хранит информаци о польователях, зарегистрированных в банке
        public List<Account> accounts { get; set; } //Коллекция Счета - хранит информацию о счетах польователей,
                                                   // у каждого пользователя может быть несколько счетов.
        public List<History> histories { get; set; }

        private void FillUsers()
        {
            users = new List<User>()
            {
                new User
                {
                    Id = 1,
                    FirsrtName = "Иван",
                    LastName = "Петров",
                    MidleName = "Сергеевич",
                    Phone = "89251234566",
                    DocNumber = "12356",
                    DocSeria = "3335",
                    Login = "Iv1",
                    Password = "assdsf@%!fghfjh4",
                    RegDate = DateTime.Parse("12.02.2000")
                },

                new User
                {
                    Id = 2,
                    FirsrtName = "Семен",
                    LastName = "Петров",
                    MidleName = "Сергеевич",
                    Phone = "89251244566",
                    DocNumber = "12356",
                    DocSeria = "3335",
                    Login = "Sm1",
                    Password = "aeredsf@%!fghfjh4",
                    RegDate = DateTime.Parse("15.03.2001")
                },

                new User
                {
                    Id = 3,
                    FirsrtName = "Алексей",
                    LastName = "Сидоров",
                    MidleName = "Алексеич",
                    Phone = "89255464566",
                    DocNumber = "1233356",
                    DocSeria = "334435",
                    Login = "AA1",
                    Password = "asdfdsf@%!fghfjh4",
                    RegDate = DateTime.Parse("23.05.2001")
                },
            };
        }
        private void FillAccounts()
        {
            accounts = new List<Account>()
            {
                new Account
                {
                    Id = 1,
                    OwnerId = 1,
                    CreateDate = DateTime.Parse("13.02.2000"),
                    CurrentSum = Decimal.Parse("2300089", System.Globalization.NumberStyles.Any,
                        CultureInfo.InvariantCulture)
                },
                new Account
                {
                    Id = 2,
                    OwnerId = 1,
                    CreateDate = DateTime.Parse("17.06.2010"),
                    CurrentSum = Decimal.Parse("6230000", System.Globalization.NumberStyles.Any,
                        CultureInfo.InvariantCulture)
                },
                new Account
                {
                    Id = 3,
                    OwnerId = 2,
                    CreateDate = DateTime.Parse("16.03.2001"),
                    CurrentSum = Decimal.Parse("5500", System.Globalization.NumberStyles.Any,
                        CultureInfo.InvariantCulture)
                },
                new Account
                {
                    Id = 4,
                    OwnerId = 3,
                    CreateDate = DateTime.Parse("24.05.2001"),
                    CurrentSum = Decimal.Parse("44455080", System.Globalization.NumberStyles.Any,
                        CultureInfo.InvariantCulture)
                },
                new Account
                {
                    Id = 5,
                    OwnerId = 3,
                    CreateDate = DateTime.Parse("24.05.2011"),
                    CurrentSum = Decimal.Parse("4443355080", System.Globalization.NumberStyles.Any,
                        CultureInfo.InvariantCulture)
                },
                new Account
                {
                    Id = 6,
                    OwnerId = 3,
                    CreateDate = DateTime.Parse("24.05.2016"),
                    CurrentSum = Decimal.Parse("35500,80", System.Globalization.NumberStyles.Any,
                        CultureInfo.InvariantCulture)
                },
            };
        }

        private void FillHistory()
        {
            int k = 0;

            int getId()
            {
                k++;
                return k;
            }

            histories = new List<History>()
            {
                new History()
                {
                    Id = getId(),
                    Operation = AccountMoneyOperation.oIn,
                    AccountId = 1,
                    CreateDate = DateTime.Parse("26.05.2008"),
                    TransferSum = Decimal.Parse("2000.00", System.Globalization.NumberStyles.Any,
                        CultureInfo.InvariantCulture)
                },
                new History()
                {
                    Id = getId(),
                    Operation = AccountMoneyOperation.oIn,
                    AccountId = 1,
                    CreateDate = DateTime.Parse("27.05.2008"),
                    TransferSum = Decimal.Parse("5000.00", System.Globalization.NumberStyles.Any,
                        CultureInfo.InvariantCulture)
                },
                new History()
                {
                    Id = getId(),
                    Operation = AccountMoneyOperation.oOut,
                    AccountId = 1,
                    CreateDate = DateTime.Parse("28.05.2008"),
                    TransferSum = Decimal.Parse("4000.00", System.Globalization.NumberStyles.Any,
                        CultureInfo.InvariantCulture)
                },
                new History()
                {
                    Id = getId(),
                    Operation = AccountMoneyOperation.oOut,
                    AccountId = 1,
                    CreateDate = DateTime.Parse("28.05.2008"),
                    TransferSum = Decimal.Parse("2000.00", System.Globalization.NumberStyles.Any,
                        CultureInfo.InvariantCulture)
                },

                new History()
                {
                    Id = getId(),
                    Operation = AccountMoneyOperation.oIn,
                    AccountId = 3,
                    CreateDate = DateTime.Parse("27.05.2008"),
                    TransferSum = Decimal.Parse("15000.00", System.Globalization.NumberStyles.Any,
                        CultureInfo.InvariantCulture)
                },
                new History()
                {
                    Id = getId(),
                    Operation = AccountMoneyOperation.oOut,
                    AccountId = 3,
                    CreateDate = DateTime.Parse("29.05.2008"),
                    TransferSum = Decimal.Parse("7000.00", System.Globalization.NumberStyles.Any,
                        CultureInfo.InvariantCulture)
                },
            };
        }

        public void FillTestData()
        {
            FillUsers();
            FillAccounts();
            FillHistory();
        }
    }
}