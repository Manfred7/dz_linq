﻿using System;
using System.Linq;

namespace dz_linq
{
    class ModelRuner
    {
        private model testModel;

        public ModelRuner()
        {
            testModel = new model();
        }

        public void FillTestData()
        {
            testModel.FillTestData();
        }

        //1. Вывод информации о заданном аккаунте по логину и паролю
        public void RunTask1()
        {
            var findedUsers = from u in testModel.users
                where (u.Login == "Iv1" & u.Password == "assdsf@%!fghfjh4")
                select new
                {
                    u.Id,
                    u.Login,
                    u.Password,
                    u.FirsrtName,
                    u.LastName,
                    u.Phone,
                    u.RegDate
                };

            foreach (var tempUser in findedUsers)
            {
                Console.WriteLine($"{tempUser.Id} -" +
                                  $" {tempUser.FirsrtName} -" +
                                  $"  {tempUser.LastName} -" +
                                  $" {tempUser.Phone} -" +
                                  $" {tempUser.RegDate.ToShortDateString()}");
            }
        }

        //2. Вывод данных о всех счетах заданного пользователя
        public void RunTask2()
        {
            var UserAccsInfo = from u in testModel.users
                where (u.Login == "Iv1" & u.Password == "assdsf@%!fghfjh4")
                join a in testModel.accounts on u.Id equals a.OwnerId
                select new
                {
                    CreateDate = a.CreateDate,
                    CurrentSum = a.CurrentSum,
                    AccId = a.Id,
                    AccOwner = string.Format("{0} {1} {2}", u.LastName, u.FirsrtName, u.MidleName)
                };

            foreach (var accInfo in UserAccsInfo)
            {
                Console.WriteLine(
                    $"Id счета: {accInfo.AccId}\n" +
                    $" ФИО владельца счета: {accInfo.AccOwner}\n " +
                    $"Дата создания: {accInfo.CreateDate.ToShortDateString()}\n" +
                    $" Сумма на счете: {accInfo.CurrentSum}\n");
            }
        }

        //3. Вывод данных о всех счетах заданного пользователя, включая историю по каждому счёту
        public void RunTask3(string userLogin)
        {
            var result = from a in testModel.accounts
                join h in testModel.histories on a.Id equals h.AccountId
                join u in testModel.users on a.OwnerId equals u.Id
                where (u.Login == userLogin)
                select new
                {
                    acc_created = a.CreateDate,
                    a.CurrentSum,
                    acc_id = a.Id,
                    h.Operation,
                    h.TransferSum,
                    u.LastName,
                    u.FirsrtName
                };
            foreach (var rec in result)
            {
                Console.WriteLine($"Владелец счета: {rec.LastName} {rec.FirsrtName} |" +
                                  $"Id счета: {rec.acc_id} | " +
                                  $" Дата открытия счета: {rec.acc_created.ToShortDateString()} | " +
                                  $"Операция: {rec.Operation} | " +
                                  $"Текущая сумма на счете: {rec.CurrentSum} | " +
                                  $"Сумма по операции: {rec.TransferSum} ");
            }
        }

        //4. Вывод данных о всех операциях пополенения счёта с указанием владельца каждого счёта
        public void RunTask4()
        {
            var result = from h in testModel.histories
                join a in testModel.accounts on h.AccountId equals a.Id
                join u in testModel.users on a.OwnerId equals u.Id
                where (h.Operation == AccountMoneyOperation.oIn)
                select new
                {
                    h.CreateDate,
                    h.TransferSum,
                    h.AccountId,
                    u.LastName,
                    u.FirsrtName
                };

            foreach (var rec in result)
            {
                Console.WriteLine($"Дата операции: {rec.CreateDate.ToShortDateString()} " +
                                  $"Владелец счета: {rec.LastName} {rec.FirsrtName} " +
                                  $"Приход стредства: {rec.TransferSum}");
            }
        }

        //5. Вывод данных о всех пользователях у которых на счёте сумма больше N (N задаётся из вне и может быть любой)
        public void RunTask5(decimal sumFilter)
        {
            var result = from a in testModel.accounts
                join u in testModel.users on a.OwnerId equals u.Id
                where (a.CurrentSum > sumFilter)
                select new
                {
                    u.LastName,
                    u.FirsrtName,
                    accId = a.Id,
                    a.CurrentSum
                };

            Console.WriteLine($"Счета с суммой превышающей {sumFilter}:");

            foreach (var rec in result)
            {
                Console.WriteLine($"Владелец счета:{rec.LastName} {rec.FirsrtName}" +
                                  $" Id счета:{rec.accId}" +
                                  $" Сумма на счете: {rec.CurrentSum} ");
            }
        }
    }
}