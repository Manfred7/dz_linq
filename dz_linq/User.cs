﻿using System;

namespace dz_linq
{
    public class User
    {
        public int Id { get; set; }
        public string FirsrtName { get; set; }
        public string LastName { get; set; }
        public string MidleName { get; set; }
        public string Phone { get; set; }
        public string DocNumber { get; set; }
        public string DocSeria { get; set; }
        public DateTime RegDate { get; set; }
        public string Login { get; set; }
        public string Password { get; set; }

        public override string ToString()
        {
            return String.Format("ID = {0} FirsrtName= {1} LastName={2} MidleName={3}",
                Id, FirsrtName, LastName, MidleName);
        }
    }

}