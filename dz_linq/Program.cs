﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace dz_linq
{
    internal class Program
    {
        public static void Main(string[] args)
        {
            ModelRuner mr = new ModelRuner();
            mr.FillTestData();

            Console.WriteLine("1. Вывод информации о заданном аккаунте по логину и паролю");
            mr.RunTask1();
            Console.ReadKey();

            Console.WriteLine();
            Console.WriteLine("2. Вывод данных о всех счетах заданного пользователя");
            mr.RunTask2();
            Console.ReadKey();

            Console.WriteLine();
            Console.WriteLine("3. Вывод данных о всех счетах заданного пользователя, включая историю по каждому счёту");
            string userLogin = "Iv1";
            mr.RunTask3(userLogin);
            Console.ReadKey();

            Console.WriteLine();
            Console.WriteLine("4. Вывод данных о всех операциях пополенения счёта с указанием владельца каждого счёта");
            mr.RunTask4();
            Console.ReadKey();

            Console.WriteLine();
            Console.WriteLine("5. Вывод данных о всех пользователях у которых на счёте сумма больше N (N задаётся из вне и может быть любой");
            decimal N = 100_000;
            mr.RunTask5(N);
        }
    }
}