﻿using System;

namespace dz_linq
{
    public enum AccountMoneyOperation
    {
        oIn, //приход денег   
        oOut //вывод денег
    }
    public class History
    {
        public int Id { get; set; }
        public DateTime CreateDate { get; set; }
        public AccountMoneyOperation Operation { get; set; }
        public decimal TransferSum { get; set; }
        public int AccountId { get; set; }
    }
}